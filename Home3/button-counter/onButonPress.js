
function func() {
    let count = 0;
    return function() {
        return count++;
    };
};

const buttons = document.querySelectorAll('html body .btn button')

buttons.forEach(function(btn) {

    let btnClick = func();

    btn.addEventListener('click', function() {
        console.log(btnClick());
    });
});
